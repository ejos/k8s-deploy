![](images/k8s-golang.png)

# Create, Update, List, Delete & Expose a Deployment using Kubernetes-Api

## Table of Content
- [Project description](#project-description)
- [How to use it - For Devs](#how-to-use-it---for-devs)
  - [1.- Prerequisites](#1--prerequisites)
  - [2.- Environment Variables that you must set](#2--environment-variables-that-you-must-set)
    - [Terminal Console](#terminal-console)
    - [Gitlab](#gitlab)
      - [Project level](#project-level)
      - [Pipeline level:](#pipeline-level)
  - [3.- Create a new branche on your gitlab repository](#3--create-a-new-branche-on-your-gitlab-repository)
  - [4.- Create/Update a .gitlab-ci.yml file](#4--createupdate-a-gitlab-ciyml-file)
  - [5.-Load configurations from json files (optional)](#5-load-configurations-from-json-files-optional)
    - [Deployment configurations](#deployment-configurations)
    - [Configmaps aka Environment variables for the app](#configmaps-aka-environment-variables-for-the-app)
    - [Secrets aka Sensitive Environment variables for the app](#secrets-aka-sensitive-environment-variables-for-the-app)
  - [6.- Update configurations from environment variables - WIP](#6--update-configurations-from-environment-variables---wip)
    - [Update secrets](#update-secrets)
    - [Update configmaps](#update-configmaps)
- [How to operate it - For DevOps](#how-to-operate-it---for-devops)
  - [Step Cero (0)](#step-cero-0)
  - [How is this work](#how-is-this-work)
  - [Environment Variables that you must set](#environment-variables-that-you-must-set)
    - [Administration level:](#administration-level)
  - [Examples](#examples)
    - [Create a Deployment from **command line**](#create-a-deployment-from-command-line)
    - [Create a Configmap from **command line**](#create-a-configmap-from-command-line)
    - [Create a Secret from **command line**](#create-a-secret-from-command-line)
    - [Delete a Deployment from **command line**](#delete-a-deployment-from-command-line)
  - [Throubleshooting](#throubleshooting)
  - [Documentation that you must check out](#documentation-that-you-must-check-out)


## How to use it

### 1.- Prerequisites

You MUST have at least one docker image on the [gitlab](https://gitlab.com) project container registry or [docker hub](https://hub.docker.com)

### 2.- Environment Variables that you must set

#### Terminal Console

* **CI_REGISTRY_IMAGE**: The location of the docker container image
* **ENV_NAMESPACE**: Name of the **Namespace** where you want to make the deployment
* **ENV_HTTP_PORT**: Port used by APP
* **ENV_APP_NAME**: Name of the APP
* **ENV_BRANCH_NAME**: Name of the branch, ex: "master"
* **TAG**: Docker commit image tag, ex: "latest"
* **ENV_K8S_DEPLOY**: Action that it will be take: config | secret | app
* **ENV_SECRET_REGISTER_NAME**: In case the image are on a private docker registry

#### Gitlab

##### Project level 

* **ENV_NAMESPACE**: Name of the **Namespace** where you want to make the deployment
* **ENV_HTTP_PORT**: Port used by APP
* **ENV_APP_NAME**: Name of the APP

##### Pipeline level:

example [gitlab-ci.yml](examples/.gitlab-ci.yml) on _deploy-app_ stage

* **ENV_BRANCH_NAME**: Name of the **branch** where the image come from, _default_: master
* **TAG**: Version of the docker image, _default_: latest
* **ENV_K8S_DEPLOY**: Action that it will be take: config | secret | app

### 3.- Create a new branche on your gitlab repository

* Example: k8s-deploy

### 4.- Create/Update a [.gitlab-ci.yml](examples/.gitlab-ci.yml) file

```
stages:
    - build
    - deploy
  
  build:
    stage: build
    image: docker:19.03.1
    only:
      refs:
        - master
      changes:
        - Dockerfile
        - src/**/*
    script:
      - docker build -t $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:latest .
      - docker tag $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:latest $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
      - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
      - docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
      - docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:lates
    tags:
      - gcp-docker-runner
  
  deploy:
    stage: deploy
    only:
      refs:
        - k8s-deploy
      changes:
        - not-existing-folder
    when: manual
    script:
      - /usr/local/bin/k8s-deploy
    tags:
      - k8s-shell-runner
  
```

This file contains 2 stages

* **build**: for building the docker image
* **deploy**: It will be used for deploying k8s secrets, configmaps or the app when the pipeline is manually triggered on the branch **k8s-deploy**

Changes directive is used to make sure the jobs dont get trigger by code updates

```
changes:
  - not-existing-folder
```

### 5.-Load configurations from json files (optional)

#### Deployment configurations 
1. k8s-deploy is going to check for the file path ```k8s-configs/deployment-config.json```
2. If file exists is going to load the following configurations:
    * **Replicas**: Number of pods replicas (default 2)
    * **MaxUnavailable**: The maximum number of pods that can be unavailable during the update (default 50%)
    * **MaxSurge**: The maximum number of pods that can be scheduled above the desired number of pods. (default 50%)
    * **SecretsRefName**: Name of the secrets (**sensitive environment variables**) to be used with the applications.
    * **InitialDelaySeconds**: Number of seconds after the container has started before liveness probes are initiated
    * **PeriodSeconds**: How often (in seconds) to perform the probe
    * **Path** && **Port**: Path and port used for the http get method probe
If this value is setted and not found on kubernetes the deployment will fail.

[deployment json file example](examples/k8s-configs/deployment-config.json)
```
{
  "Pod": {
    "Replicas": 2,
    "UpdateStrategy": {
      "RollingUpdate": {
        "MaxUnavailable": "50%",
        "MaxSurge": "80%"
      }
    },
    "Container": {
      "SecretsRefName": "",
      "Probes": {
        "ReadinessProbe" : {
          "InitialDelaySeconds": 3,
          "PeriodSeconds": 3,
          "HttpGet" : {
            "Path" : "/",
            "Port" : 3000
          }
        }
      }
    }
  }
}
```

#### Configmaps aka Environment variables for the app

1. k8s-deploy is going to check for the file path ```k8s-configs/configmap.json```
2. If file exists is going to create/update the configmap considering:
    * store_name: Name of the configmap to create or update if exist
    * configmaps: Array of key-value (environment variables available to the app)

[configmap json file example](examples/k8s-configs/configmap.json)
```
{
  "store_name": "configmap-test",
  "configmaps": {
    "PORT": "3000",
    "COLOR": "VERDE",
    "TYPE": "NA",
    "TEST_ENV": "Work it Harder, Make it Better Do it Faster, Makes us Stronger"
  }
}
```

#### Secrets aka Sensitive Environment variables for the app

1. k8s-deploy is going to check for environment variable ```SECRET``` that must be set when the pipeline is manually triggered on the branch deploy-secrets
2. If exist and not empty will create/update a secret in kubernetes

SECRET environment variable example (json format)
```
{
  "store_name": "secrets-ref-test",
  "secrets": {
    "DB_USER": "DaftPunk",
    "DB_PASSWD": "Work it Harder, Make it Better, Do it Faster, Makes us Stronger"
  }
}
```

* store_name: Name of the secret to create or update if exist
* secrets: Array of key-value (sensitive environment variables available to the app)

### 6.- Update configurations from environment variables - WIP

#### Update secrets
When the **secrets** is going to be upgrated via **deploy-secret** it has to take on count that if there exist a previos **configmaps** deployed then the file ``` configmaps.json ``` it must exist on the branch **deploy-secret** before the pipeline run

#### Update configmaps
In case that the **configmaps** is going to be upgrated via **deploy-config** it has to take on count that if there exist a previos **secret** deployed then the file ``` deployment.json ``` it must exist on the branch **deploy-config** before the pipeline run


## How to operate it - For DevOps

In case that the *Deploy* exist the script it will update the *image* replacing the *tag* with the last *commit* of your project, and automatically it will delete the pods for *re-create* the *deployment* with the latest **image builded**.

### How is this work
1. Just clone the project:
``` git clone _url_of_k8s_deploy_git ```

2. Go to the cloned project:
```
 cd _k8s_deploy_git 
```
3. Build Golang Script:
```
  make build && make install && make clean
```
4. Set the Environment Variables:
 Check out [environment variables](#environment-variable-that-it-must-be-set-before-execute-this-helper) for that

### Environment Variables that it must be set before execute this helper

* ENV_APP_NAME="test"
* ENV_BRANCH_NAME="master"
* TAG="1.12"
* ENV_NAMESPACE="default"
* ENV_HTTP_PORT="8080"
* CI_REGISTRY_IMAGE: "docker-registry"

### Examples

#### Create a Deployment from **command line**

1. Set variables
* ENV_APP_NAME="test"
* ENV_BRANCH_NAME="master"
* TAG="1.12"
* ENV_NAMESPACE="default"
* ENV_HTTP_PORT="8080"
* CI_REGISTRY_IMAGE: "docker-registry"
* ENV_DEPLOY_NAME: "app"

```

export  ENV_APP_NAME CI_REGISTRY_IMAGE CI_COMMIT_SHORT_SHA ENV_NAMESPACE ENV_HTTP_PORT ENV_SECRET_REGISTER_NAME ENV_BRANCH_NAME
```

2. Run **Script** 

``` 
./k8s-deploy
```

#### Create a Configmap from **command line**

1. Set variables

```
ENV_APP_NAME="test"
ENV_BRANCH_NAME="master"
TAG="1.12"
ENV_NAMESPACE="default"
ENV_HTTP_PORT="8080"

ENV_DEPLOY_NAME: "config"
export  ENV_APP_NAME CI_REGISTRY_IMAGE CI_COMMIT_SHORT_SHA ENV_NAMESPACE ENV_HTTP_PORT ENV_SECRET_REGISTER_NAME ENV_BRANCH_NAME
```

2. Configmap json file must exist on the relative path ```k8s-configs/configmap.json```

3. Run **Script** 

``` 
./k8s-deploy
```

#### Create a Secret from **command line**

1. Set variables
* ENV_APP_NAME="test"
* ENV_BRANCH_NAME="master"
* TAG="1.12"
* ENV_NAMESPACE="default"
* ENV_HTTP_PORT="8080"
* CI_REGISTRY_IMAGE: "docker-registry"

* ENV_DEPLOY_NAME: "secret"
* SECRET='{"store_name":"secrets-ref-test","secrets":{"PORT": "3000", "COLOR": "BROWN"}}'

```
export  ENV_APP_NAME CI_REGISTRY_IMAGE CI_COMMIT_SHORT_SHA ENV_NAMESPACE ENV_HTTP_PORT ENV_SECRET_REGISTER_NAME ENV_BRANCH_NAME SECRET

```

2. Run **Script** 

``` 
./k8s-deploy
```

#### Delete a Deployment from **command line**

```
./k8s-deploy delete

Building deployment  deploy-go-testwcqw  ...
*v1.deployments
Deleting deployment...
Deleted deployment.
```
### Administration Purpose
Make sure you have a Kubernetes cluster and kubectl is configured:
```
  kubectl cluster-info
  kubectl get pods
```
If that doesn't work, remit to [How to get gke credentials](#documentation-that-you-must-check-out) and try again

#### Variable on Administration level:
* **ENV_SLACK_WEBHOOK_TOKEN_URL**: In case that exist a Channel, it could be set the webhook using this variable, more info [here](https://api.slack.com/messaging/webhooks)
* **ENV_DELETE_DEPLOYMENT_NAME**: Name of the **deployment** that will be **deleting**
* **ENV_SECRET_REGISTER_NAME**: Name of the **Secret** that will allow pull image from custom repository
* **CI_REGISTRY_IMAGE**: In case that deploy from **command line**

### Throubleshooting
```
Log file created at: 2020/08/05 05:49:49
Running on machine: gke-admin
Binary: Built with gc go1.14.6 for linux/amd64
Log line format: [IWEF]mmdd hh:mm:ss.uuuuuu threadid file:line] msg
F0805 05:49:49.453823   20353 k8s-deploy.go:62] Could not read kubeconfig /home/user/.kube/config: no Auth Provider found for name "gcp" 
```

You must uncomment the *auth* plugin that you need, and **build** the script again check out [How is this work](#how-is-this-work)

### Documentation that you must check out
* [How to client-go](https://github.com/kubernetes/client-go)
* [How to enable-go-modules](https://github.com/kubernetes/client-go/blob/master/INSTALL.md#enabling-go-modules)
* [How to get gke credentials](https://cloud.google.com/sdk/gcloud/reference/container/clusters/get-credentials)
