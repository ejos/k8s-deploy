# This how we want to name the binary output
BINARY="k8s-deploy"

# These are the values we want to pass for VERSION and BUILD
# git tag 1.0.1
# git commit -am "One more change after the tags"
GOBASE=$(shell pwd)
GOROOT="$(GOBASE)/src"
GOBIN="$(GOPATH)/bin"
GOPATH="/usr/local/go"
GOBIN="$(GOPATH)/bin"
VERSION=`git describe --tags`
EMAIL=`git config user.email`
SHA=`git log --pretty=format:'%H' -n 1`
USERNAME="Edward_Ortega"
BUILD=`date +%FT%T%z`

MAKEFLAGS += --silent

# Setup the -ldflags option for go build here, interpolate the variable values
LDFLAGS=-ldflags "-w -s -X main.version=${VERSION} -X main.build=${BUILD} -X main.email=${EMAIL} -X main.gitsha=${SHA}"

configure: 
	go-get

## Builds the project
build: 
	go build ${LDFLAGS} -o ${BINARY} ${GOROOT}

## Installs our project: copies binaries
install: 
	cp ${BINARY} ${GOBIN}

## Cleans our project: deletes binaries
clean: 
	if [ -f ${BINARY} ] ; then rm ${BINARY} ; fi

go-get:
	go get 

.PHONY: clean install
