package main

import (
    "bytes"
    "encoding/json"
    "errors"
    "github.com/rs/zerolog/log"
    "net/http"
    "time"
)


type SlackRequestBody struct {
    Text string `json:"text"`
}


type SlackMessageBody struct {
   Text string `json:"text"`
   Attachments []Attachment `json:"attachments"`
}

type Attachment struct {
	Title    string   `json:"title"`
	Text     string   `json:"text"`
	MrkdwnIn []string `json:"mrkdwn_in"`
	Fields   []Field  `json:"fields"`
}

type Field struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short"`
}

/* notify slack */
func notifySlack( m map[string]string ) {
   log.Info().
       Str("Deployment", m["deployName"]).
       Str("SlackWebHook", m["slackWebHookTokenUrl"]).
       Msg("notifying Slack")

    webhookUrl := m["slackWebHookTokenUrl"]

    message := buildMessage( m )

    err := SendSlackNotification(webhookUrl, message )

    if err != nil {
        panic(err)
    }
}

func buildMessage(m map[string]string) SlackMessageBody {

    message := SlackMessageBody{}

    message = SlackMessageBody{
           Attachments: []Attachment{
             { Title: m["slackMessage"] +"\t\n", MrkdwnIn: []string{ "text"},
               Fields: []Field{
		  { Title: "Deployment:" , Value: m["deployName"], Short: true},
               },
             },
           },
        }
    return message
}

func SendSlackNotification(webhookUrl string,  msg SlackMessageBody ) error {

    slackBody, _ := json.Marshal( msg )
    req, err := http.NewRequest(http.MethodPost, webhookUrl, bytes.NewBuffer(slackBody))
    if err != nil {
        return err
    }

    req.Header.Add("Content-Type", "application/json")

    client := &http.Client{Timeout: 10 * time.Second}
    resp, err := client.Do(req)
    if err != nil {
        return err
    }

    buf := new(bytes.Buffer)
    buf.ReadFrom(resp.Body)
    if buf.String() != "ok" {
        return errors.New("Non-ok response returned from Slack")
    }
    return nil
}
