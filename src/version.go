package main

import (
	"github.com/rs/zerolog/log"
)

var version string
var build string
var gitsha string
var email string

func logVersion() {
     log.Info().
	Str("version", version).
	Str("build", build).
	Str("sha", gitsha ).
	Str("email", email ).
	Msg("K8s-deploy info")
}
