package main

import (
	"context"
	"encoding/json"
	"github.com/rs/zerolog/log"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type SecretsJson struct {
	Name    string            `json:"store_name"`
	Secrets map[string]string `json:"secrets"`
}

/* create/update secrets */
func createSecrets(m map[string]string) string  {

	var config SecretsJson

	jsonData := m["jsondata"]

	jsonError := json.Unmarshal([]byte(jsonData), &config)

	if jsonError != nil {
		log.Error().
			Str("JsonData", jsonData).
			Msg("Failed to parse json data")
		panic(jsonError)
	}
	log.Info().
		Str("SecretName", config.Name).
		Str("Namespace", m["namespace"]).
		Msg("Building secret")

	secret := apiv1.Secret{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Secret",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      config.Name,
			Namespace: m["namespace"],
		},
		StringData: config.Secrets,
	}

	if _, err := clientset.CoreV1().Secrets(m["namespace"]).Get(context.TODO(), config.Name, metav1.GetOptions{}); errors.IsNotFound(err) {
		_, createErr := clientset.CoreV1().Secrets(m["namespace"]).Create(context.TODO(), &secret, metav1.CreateOptions{})

		if createErr != nil {
			log.Error().Msg("Failed to create secret")

			panic(err)
		} else {
			log.Info().Msg("Secret created")
			if ( action == "secret" ){
			   log.Info().Msg("Calling Updating Deploy " + action)
	                   m["secretHash"] = createSha256Hash( m )
		           m["secretRefName"] = config.Name
	                   if checkDeploymentExistByAppName(m) {
		              createDeploy(deployConfigs, m)
			   }
			}
		}
	} else {
		updatingSecrets( m )
	}

	return config.Name
}

// Updating secrets
func updatingSecrets(m map[string]string) {

	var config SecretsJson

	jsonData := m["jsondata"]

	jsonError := json.Unmarshal([]byte(jsonData), &config)

	if jsonError != nil {
		log.Error().
			Str("JsonData", jsonData).
			Msg("Failed to parse json data")
		panic(jsonError)
	}

	log.Info().
		Str("SecretName", config.Name).
		Msg("Building secret")

	secret := apiv1.Secret{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Secret",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      config.Name,
			Namespace: m["namespace"],
		},
		StringData: config.Secrets,
	}

	_, updateErr := clientset.CoreV1().Secrets(m["namespace"]).Update(context.TODO(), &secret, metav1.UpdateOptions{})

	if updateErr != nil {
		log.Error().Msg("Failed to update secret")
			panic(updateErr)
	} else {
	  log.Info().Msg("Secret updated")
	  if ( action == "secret" ){
	     m["secretHash"] = createSha256Hash( m )
	     m["secretRefName"] = config.Name
	     if checkDeploymentExistByAppName(m) {
                if checkConfigMapExistByName(m) {
	           m["configHash"] = createSha256Hash( m )
		}
	        log.Info().Msg("Calling Updating Deploy " + action)
		createDeploy(deployConfigs, m)
	     }
	  }
       }
}

func checkSecretRefExist(m map[string]string, config DeploymentConfig) bool {

	log.Info().Msg("Checking if Secret exist")
	_, error := clientset.CoreV1().Secrets(m["namespace"]).Get(context.TODO(), config.Pod.Container.SecretsRefName, metav1.GetOptions{})

	if error != nil {
		log.Error().
			Str("SecretsRefName", config.Pod.Container.SecretsRefName).
			Str("Namespace", m["namespace"]).
			Msg("Secrets ref name not found on k8s")

		panic(error)
	}

	return true
}

func checkSecretRefExistByName(m map[string]string) bool {

	log.Info().Msg("Checking if ConfigMaps exist ")
        config := loadDeploymentConfigs()
	_, error := clientset.CoreV1().Secrets(m["namespace"]).Get(context.TODO(), config.Pod.Container.SecretsRefName, metav1.GetOptions{})

	if error != nil {
		log.Error().
			Str("Secret", config.Pod.Container.SecretsRefName).
			Str("Namespace", m["namespace"]).
			Msg("SecretRef Name not found on k8s")

		panic(error)
	}

	return true
}
