/*
Copyright 2017 The Kubernetes Authors.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Note: the example only works with the code within the same release/branch.
package main

import (
	"flag"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"math/rand"
	"os"
	"path/filepath"
	"time"
)

const k8sConfigFolder = "k8s-configs"

var deployConfigs DeploymentConfig
var clientset *kubernetes.Clientset
var action string


// init func
func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	err := godotenv.Load()
	if err == nil {
		log.Info().
			Str("envFilePath", ".env").
			Msg("Environment variables loaded from file")
	}
}

// main func
func main() {

	if len(os.Args) > 1  && os.Args[1] == "version" {
			logVersion()
	} else if len(os.Args) > 1  && os.Args[1] == "delete" {
			deleteDeploy()
	} else {

	    envs := []string{"ENV_K8S_DEPLOY"}
	    if checkEnv(envs) == true {

                    action = os.Getenv("ENV_K8S_DEPLOY")

		    // sets k8s global clientset
		    clientset = getK8sClientSet()
		    deployConfigs = loadDeploymentConfigs()

		    if action == "app" {
			deployApp()
		    } else if action == "config" {
			deployConfig()
		    } else if action == "secret" {
			deploySecret()
		    }
	      }
	}
}


/*	Returns k8s clientset for operations */
func getK8sClientSet() *kubernetes.Clientset {

	var kubeconfig *string
	home := homedir.HomeDir()
	kubeConfigPath := filepath.Join(home, ".kube", "config")

	if home == "" {
		home = os.Getenv("HOME")
	}

	kubeConfigPath = filepath.Join(home, ".kube", "config")

	log.Info().
		Str("path", kubeConfigPath).
		Msg("Kubeconfig loaded from file")

	kubeconfig = flag.String("kubeconfig", kubeConfigPath, "absolute path to the kubeconfig file")

	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err)
	}

	clientset, err := kubernetes.NewForConfig(config)

	if err != nil {
		log.Info().
			Str("kubeconfig", *kubeconfig).
			Msg("Could not read kubeconfig")

		panic(err)
	}

	return clientset
}

/*	Load Map app to kubernetes */
func loadMap() map[string]string {

	rand.Seed(time.Now().Unix())
	envs := []string{"ENV_SECRET_REGISTER_NAME", "ENV_NAMESPACE", "TAG", "ENV_BRANCH_NAME", "ENV_APP_NAME", "ENV_HTTP_PORT", "ENV_K8S_DEPLOY"}

	dep := make(map[string]string,0)

	if checkEnv(envs) == true {

		appName := os.Getenv("ENV_APP_NAME")

		random := randSuffix(4)
		deployName := "deploy-go-" + appName + "-" + random
		appName += "-app"

		dep = map[string]string{
			"namespace":     os.Getenv("ENV_NAMESPACE"),
			"secretRegName": os.Getenv("ENV_SECRET_REGISTER_NAME"),
			"imageName":     os.Getenv("CI_REGISTRY_IMAGE"),
			"imageTag":      os.Getenv("TAG"),
			"httpPort":      os.Getenv("ENV_HTTP_PORT"),
			"env":           os.Getenv("ENV_BRANCH_NAME"),
			"appName":       appName,
			"service":       appName + "-service",
			"deployName":    deployName,
		}
		dep["image"] = dep["imageName"] + "/" + dep["env"] + ":" + dep["imageTag"]
	}
	return dep
}

/*	Deploy app to kubernetes */
func deployApp() {

	dep := loadMap()
	log.Info().
		Str("Deploy-Name", dep["deployName"]).
		Str("Namespace", dep["namespace"]).
		Str("Image", dep["image"]).
		Str("Env", dep["env"]).
		Msg("Building deployment...")

	if deployConfigs.Pod.Container.SecretsRefName != "" && checkSecretRefExist(dep, deployConfigs) == true {
		dep["secretRefName"] = deployConfigs.Pod.Container.SecretsRefName
	        dep["secretHash"] = createSha256Hash( dep )
	}

	envs := []string{"ENV_SLACK_WEBHOOK_TOKEN_URL"}
	if checkEnv(envs) {
		dep["slackWebHookTokenUrl"] = os.Getenv("ENV_SLACK_WEBHOOK_TOKEN_URL")
	}

	createDeploy(deployConfigs, dep)
}

/* deploy configmap to kubernetes */
func deployConfig() {

	dep := loadMap()

	jsonPath := filepath.FromSlash(k8sConfigFolder + "/" + configMapsJson)

	exist := checkK8sConfigJsonExist(jsonPath)

	if exist == true {

	   log.Info().
		  Str("jsonPath", jsonPath).
		  Msg("Configmap loaded from json file")

	   dep["jsonpath"] =  jsonPath

	   createConfigMaps(dep)

        } else {
		log.Error().
			Str("jsonPath", jsonPath).
			Msg("Configmap json file not found")
	}
}

/* deploy secret to kubernetes */
func deploySecret() {

	dep := loadMap()

	envs := []string{"SECRET"}

	if checkEnv(envs) == true {

		dep["jsondata"] = os.Getenv("SECRET")

		envs := []string{"ENV_SLACK_WEBHOOK_TOKEN_URL"}
		if checkEnv(envs) {
			dep["slackWebHookTokenUrl"] = os.Getenv("ENV_SLACK_WEBHOOK_TOKEN_URL")
		}

	        jsonPath := filepath.FromSlash(k8sConfigFolder + "/" + configMapsJson)
	        exist := checkK8sConfigJsonExist(jsonPath)

	        if exist == true {
	            dep["jsonpath"] =  jsonPath
		}

		createSecrets(dep)
	}
}

/* delete deploy on kubernetes */
func deleteDeploy() {

	envs := []string{"ENV_NAMESPACE", "ENV_APP_NAME", "ENV_DELETE_DEPLOYMENT_NAME"}

	if checkEnv(envs) == true {
		dep := map[string]string{
			"namespace":            os.Getenv("ENV_NAMESPACE"),
			"appName":              os.Getenv("ENV_APP_NAME") + "-app",
			"deleteDeploymentName": os.Getenv("ENV_DELETE_DEPLOYMENT_NAME"),
		}
		envs := []string{"ENV_SLACK_WEBHOOK_TOKEN_URL"}
		if checkEnv(envs) {
			dep["slackWebHookTokenUrl"] = os.Getenv("ENV_SLACK_WEBHOOK_TOKEN_URL")
		}

		deleteDeployment(deployConfigs, dep)
	}
}
