![](images/k8s-golang.png)

# Create, Update, List, Delete & Expose a Deployment using Kubernetes-Api

## Table of Content
- [K8s Client](#k8s-client)
  - [Table of Content](#table-of-content)
  - [Step Cero (0)](#step-cero-0)
  - [How is this work](#how-is-this-work)
  - [Install Golang Dependencies](#install-golang-dependencies)
  - [Enviroment Variables that you must set](#environment-variables-that-you-must-set)
  - [Load configurations from json files (optional)](#load-configurations-from-json-files-optional) 
  - [Load configurations from environment variables (mandatory for secrets)](#load-configurations-from-environment-variables-mandatory-for-secrets)
  - [Examples](#examples)
    - [Create a Deployment from command line](#create-a-deployment-from-command-line)
    - [Create a Configmap from command line](#create-a-configmap-from-command-line)
    - [Create a Secret from command line](#create-a-secret-from-command-line)
  - [Create a gitlab-cicd yaml](#create-a-gitlab-cicd-yaml)
  - [Thoubleshooting](#throubleshooting)
  - [Documentation that you must check out](#documentation-that-you-must-check-out)

### K8s Client
This project allows you to interact with k8s and be able to perform the following tasks:
 *  **Create, Update, List & Delete** a **Deployment**
 *  **Create & Update** a **Configmap**
 *  **Create & Update** a **Secret**

In case that the *Deploy* exist the script it will update the *image* replacing the *tag* with the last *commit* of your project, and automatically it will delete the pods for *re-create* the *deployment* with the latest **image builded**.

### Step Cero (0)
Make sure you have a Kubernetes cluster and kubectl is configured:
```
  kubectl cluster-info
  kubectl get pods
```
If that doesn't work, remit to [How to get gke credentials](#documentation-that-you-must-check-out) and try again

### How is this work
1. Just clone the project:
``` git clone _url_of_k8s_deploy_git ```

2. Go to the cloned project:
```
 cd _k8s_deploy_git 
```
3. Build Golang Script:
```
  go build -o k8s-deploy 
```
4. Set the Environment Variables:
 Check out [environment variables](#environment-variable-that-you-must-set) for that

### Environment Variables that you must set
#### Project level or gitlab-ci.yml: 

* **ENV_NAMESPACE**: Name of the **Namespace** where you want to make the deploy
* **ENV_HTTP_PORT**: Name of the APP
* **ENV_APP_NAME**: Name of the APP
* **ENV_SECRET_REGISTER_NAME"**: Name of the **Secret** that will allow pull image from custom repository

#### Pipeline level:
* **ENV**: Name of the **branch** where the image come from, _default_: master
* **TAG**: Version of the docker image, _default_: latest

#### Administration level:
* **ENV_DELETE_DEPLOYMENT_NAME"**: Name of the **deployment** that will be **deleting**

### Load configurations from json files (optional)

#### Deployment configurations 
1. k8s-deploy is going to check for the file path ```k8s-configs/deployment-config.json```
2. If file exists is going to load the following configurations
* Replicas: Number of pods replicas (default 2)
* MaxUnavailable: The maximum number of pods that can be unavailable during the update (default 25%)
* MaxSurge: The maximum number of pods that can be scheduled above the desired number of pods. (default 25%)
* SecretsRefName: Name of the secrets (sensible environment variables) to be used with the applications.
If this value is setted and not found on kubernetes the deployment will fail.

deployment json file example
```
{
  "Replicas": 2,
  "RollingUpdate":{
    "MaxUnavailable": "25%",
    "MaxSurge": "30%"
  },
  "SecretsRefName": ""
}
```

#### Configmaps aka Environment variables for the app
1. k8s-deploy is going to check for the file path ```k8s-configs/configmap.json```
2. If file exists is going to create/update the configmap

configmap json file example
```
{
  "store_name": "configmap-test",
  "configmaps": {
    "PORT": "8081",
    "COLOR": "VERDE",
    "TYPE": "NA",
    "TEST_ENV": "Work it Harder, Make it Better Do it Faster, Makes us Stronger"
  }
}
```

* store_name: Name of the configmap to create or update if exist
* configmaps: Array of key-value (environment variables available to the app)

### Load configurations from environment variables (mandatory for secrets)

#### Secrets aka Sensible Environment variables for the app
1. k8s-deploy is going to check for environment variable ```SECRET```
2. If exist and not empty will create/update a secret in kubernetes

SECRET environment variable example (json format)
```
{
  "store_name": "lala-test",
  "secrets": {
    "DB_USER": "DaftPunk",
    "DB_PASSWD": "Work it Harder, Make it Better, Do it Faster, Makes us Stronger"
  }
}
```

* store_name: Name of the secret to create or update if exist
* secrets: Array of key-value (sensible environment variables available to the app)

### Examples

#### Create a Deployment from **command line**

1. Set variables

```
ENV_APP_NAME="test"
CI_REGISTRY_IMAGE="nginx"
TAG="1.12"
ENV="master"
ENV_NAMESPACE="default"
ENV_HTTP_PORT="80"
ENV_SECRET_NAME="docker-registry"
export  ENV_APP_NAME CI_REGISTRY_IMAGE CI_COMMIT_SHORT_SHA ENV_NAMESPACE ENV_HTTP_PORT ENV_SECRET_NAME
```

2. Run **Script** 

``` 
./k8s-deploy app
```

#### Create a Configmap from **command line**

1. Set variables

```
ENV_NAMESPACE="default"
export  ENV_NAMESPACE
```

2. Configmap json file must exist on the relative path ```k8s-configs/configmap.json```

3. Run **Script** 

``` 
./k8s-deploy config 
```

#### Create a Secret from **command line**

1. Set variables

```
ENV_NAMESPACE="default"
SECRET='{"store_name":"lala-test","secrets":{"PORT2": "lala", "COLOR2": "lolo"}}'

export  ENV_NAMESPACE SECRET
```

2. Run **Script** 

``` 
./k8s-deploy secret 
```

#### Delete a Deployment from **command line**

```
./k8s-deploy delete

Building deployment  deploy-go-testwcqw  ...
*v1.deployments
Deleting deployment...
Deleted deployment.
```
### Create a gitlab-cicd yaml
This is a part of **stage** that you need to add in case that want to build the *deployment* and *secrets* considering:
* 2 stages (deploy-app and deploy-secrets)
* 2 *protected* branches (deploy-app and deploy-secrets). FYI *if not protected will not work*
* Only is used to trigger actions only in specific branch
* Changes is usted to make sure the jobs dont get trigger by code updates
```
deploy-app:
  stage: deploy-app
  only:
    refs:
      - deploy-app
    changes:
      - not-existing-folder
  variables:
     ENV: "master"
     TAG: "latest"
  script:
    - /usr/local/bin/k8s-deploy app
  tags:
    - k8s-prd-shell-runner

deploy-secrets:
  stage: deploy-secrets
  only:
    refs:
      - deploy-secrets
    changes:
      - not-existing-folder
  variables:
    ENV: "master"
    TAG: "latest"
  script:
    - /usr/local/bin/k8s-deploy secret
  tags:
    - k8s-prd-shell-runner
```

### Throubleshooting
```
Log file created at: 2020/08/05 05:49:49
Running on machine: gke-admin
Binary: Built with gc go1.14.6 for linux/amd64
Log line format: [IWEF]mmdd hh:mm:ss.uuuuuu threadid file:line] msg
F0805 05:49:49.453823   20353 k8s-deploy.go:62] Could not read kubeconfig /home/user/.kube/config: no Auth Provider found for name "gcp" 
```

You must uncomment the *auth* plugin that you need, and **build** the script again check out [How is this work](#how-is-this-work)

### Documentation that you must check out
* [How to client-go](https://github.com/kubernetes/client-go)
* [How to enable-go-modules](https://github.com/kubernetes/client-go/blob/master/INSTALL.md#enabling-go-modules)
* [How to get gke credentials](https://cloud.google.com/sdk/gcloud/reference/container/clusters/get-credentials)
