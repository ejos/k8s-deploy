package main

import (
	"context"
	"fmt"
	"github.com/rs/zerolog/log"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/kubernetes"
)

// Create Pods
func createPodsTemplate(m map[string]string) apiv1.PodTemplateSpec {
	log.Info().
		Msg("Creating Pods Template")

	template := apiv1.PodTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			Labels: map[string]string{
				"app": m["appName"],
			},
			Annotations: map[string]string{
				"configHash": m["configHash"],
				"secretHash": m["secretHash"],
			},
		},
		Spec: apiv1.PodSpec{
			ImagePullSecrets: []apiv1.LocalObjectReference{
				{Name: m["secretRegName"]},
			},
			RestartPolicy: apiv1.RestartPolicy("Always"),
			Containers:    createContainer(m),
		},
	}
	return template
}

// Delete Pods
func deletePods(clientset *kubernetes.Clientset, m map[string]string) {
	log.Info().
		Str("deployment-name", m["deployName"]).
		Msg("Listing pods on deployment")

	deploymentsClient := clientset.CoreV1().Pods(m["namespace"])

	labelSelector := metav1.LabelSelector{
		MatchLabels: map[string]string{
			"app": m["appName"],
		},
	}

	pods, err := deploymentsClient.List(context.TODO(), metav1.ListOptions{
		LabelSelector: labels.Set(labelSelector.MatchLabels).String(),
	})
	if err != nil {
		panic(err)
	}

	deletePolicy := metav1.DeletePropagationForeground
	for _, pod := range pods.Items {
		fmt.Println("Deleting  pods:  ", pod.GetName()+" that was creating on:  ", pod.GetCreationTimestamp())
		if err := deploymentsClient.Delete(context.TODO(), pod.GetName(), metav1.DeleteOptions{
			PropagationPolicy: &deletePolicy,
		}); err != nil {
			panic(err)
		}

		log.Info().Msg("Pods deleted")
	}
}
