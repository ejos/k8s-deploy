package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/intstr"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/util/retry"
	"os"
	"path/filepath"
	"reflect"
)

const deploymentConfigJson = "deployment-config.json"

type DeploymentConfig struct {
	Pod struct {
		Replicas       int
		UpdateStrategy struct {
			RollingUpdate struct {
				MaxUnavailable string
				MaxSurge       string
			}
		}
		Container struct {
			SecretsRefName string
			Probes         struct {
				ReadinessProbe struct {
					InitialDelaySeconds int
					PeriodSeconds       int
					HttpGet             struct {
						Path string
						Port int
					}
				}
			}
		}
	}
}

/*  Load deployment configurations from
    json file if present
*/

func loadDeploymentConfigs() DeploymentConfig {
	var config DeploymentConfig

	jsonPath := filepath.FromSlash(k8sConfigFolder + "/" + deploymentConfigJson)

	exist := checkK8sConfigJsonExist(jsonPath)

	if exist == true {

		log.Info().
			Str("Source", "File").
			Str("Path", jsonPath).
			Msg("Loading deployment config")

		// Open our jsonFile
		jsonFile, err := os.Open(jsonPath)
		// if we os.Open returns an error then handle it
		if err != nil {
			panic(err)
		}
		// defer the closing of our jsonFile so that we can parse it later on
		defer jsonFile.Close()

		byteValue, _ := ioutil.ReadAll(jsonFile)

		jsonError := json.Unmarshal(byteValue, &config)

		if jsonError != nil {
			log.Error().Msg("Parsing json file: " + jsonPath + " failed")
			panic(jsonError)
		}
	 } else  {
		log.Info().
			Str("Source", "Default").
			Msg("Loading deployment config")

		config.Pod.Replicas = 2
		config.Pod.UpdateStrategy.RollingUpdate.MaxSurge = "50%"
		config.Pod.UpdateStrategy.RollingUpdate.MaxUnavailable = "50%"
		config.Pod.Container.SecretsRefName = ""
	}

	log.Info().Int("Replicas", config.Pod.Replicas).
		Str("SecretsRefName", config.Pod.Container.SecretsRefName).
		Str("RollingUpdate-MaxUnavailable", config.Pod.UpdateStrategy.RollingUpdate.MaxUnavailable).
		Str("RollingUpdate-MaxSurge", config.Pod.UpdateStrategy.RollingUpdate.MaxSurge).
		Str("Probes-Readiness-HttpGet-Path", config.Pod.Container.Probes.ReadinessProbe.HttpGet.Path).
		Int("Probes-Readiness-HttpGet-Port", config.Pod.Container.Probes.ReadinessProbe.HttpGet.Port).
		Int("Probes-Readiness-InitialDelaySeconds", config.Pod.Container.Probes.ReadinessProbe.InitialDelaySeconds).
		Int("Probes-Readiness-PeriodSeconds", config.Pod.Container.Probes.ReadinessProbe.PeriodSeconds).
		Msg("Deployment config loaded")

	return config
}

// Load Deployment
func loadDeployment(deployConfig DeploymentConfig, m map[string]string) *appsv1.Deployment {

	log.Info().
		Str("App", m["appName"]).
		Str("Deployment", m["deployName"]).
		Str("Namespace", m["namespace"]).
		Int("Replicas", deployConfig.Pod.Replicas).
		Str("SecretsRefName", deployConfig.Pod.Container.SecretsRefName).
		Str("RollingUpdate-MaxUnavailable", deployConfig.Pod.UpdateStrategy.RollingUpdate.MaxUnavailable).
		Str("RollingUpdate-MaxSurge", deployConfig.Pod.UpdateStrategy.RollingUpdate.MaxSurge).
		Msg("loadDeployment")

	deployment := appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      m["deployName"],
			Namespace: m["namespace"],
			Labels: map[string]string{
				"app": m["appName"],
			},
			Annotations: map[string]string{
				"configHash": m["configHash"],
				"secretHash": m["secretHash"],
			},
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: int32Ptr(int32(deployConfig.Pod.Replicas)),
			Strategy: appsv1.DeploymentStrategy{
				Type: "RollingUpdate",
				RollingUpdate: &appsv1.RollingUpdateDeployment{
					MaxUnavailable: &intstr.IntOrString{Type: intstr.String, StrVal: deployConfig.Pod.UpdateStrategy.RollingUpdate.MaxUnavailable},
					MaxSurge:       &intstr.IntOrString{Type: intstr.String, StrVal: deployConfig.Pod.UpdateStrategy.RollingUpdate.MaxSurge},
				},
			},
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"app": m["appName"],
				},
			},
			Template: createPodsTemplate(m),
		},
	}
	return &deployment
}

// Create Deployment
func createDeploy(deployConfig DeploymentConfig, m map[string]string) {

	m["configHash"] = createSha256Hash( m )
	deployment := loadDeployment(deployConfig, m)

	if !checkDeploymentExistByAppName(m) {
		deploymentsClient := clientset.AppsV1().Deployments(m["namespace"])

		log.Info().Msg("Creating deployment...")

		result, err := deploymentsClient.Create(context.TODO(), deployment, metav1.CreateOptions{})
		if err != nil {
			panic(err)
		}

		log.Info().
			Str("Name", result.GetObjectMeta().GetName()).
			Msg("Deployment created")

		if m["slackWebHookTokenUrl"] != "" {
			m["slackMessage"] = "DEPLOYMENT CREATED"
			notifySlack(m)
		}

		createService(m)
	} else {
		updateDeployment(deployment, m)
	}
}

// Updating Deployments
func updateDeployment(deployment *appsv1.Deployment, m map[string]string) {
	log.Info().
		Msg("Updating deployment")

	if checkDeploymentExistByAppName(m) {
		deploymentsClient := clientset.AppsV1().Deployments(m["namespace"])

		retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
			result, getErr := deploymentsClient.Get(context.TODO(), m["deployName"], metav1.GetOptions{})
			if getErr != nil {

				log.Error().Msg("Failed to get latest version of Deployment")

				panic(getErr)
			}

			if m["image"] != "" {
				log.Info().
					Str("deployment: ", m["deployName"]).
					Str("tag", m["imageTag"]).
					Msg("Updating deployment with new image tag")
				result.Spec.Template.Spec.Containers[0].Image = m["image"] // change image version
			}
			if m["secretRefName"] != "" ||  m["configmapName"] != "" {
			        confRef := getContainerEnvVars(m)
				log.Info().
					Str("deployment: ", m["deployName"]).
					Str("secret: ", m["secretRefName"]).
					Str("configmapName: ", m["configmapName"]).
					Msg("Updating deployment with the values")
				result.Spec.Template.ObjectMeta.Annotations["secretHash"] = m["secretHash"]
				result.Spec.Template.ObjectMeta.Annotations["configHash"] = m["configHash"]
				result.Spec.Template.Spec.Containers[0].EnvFrom = confRef
			}
			_, updateErr := deploymentsClient.Update(context.TODO(), result, metav1.UpdateOptions{})
			return updateErr
		})

		if retryErr != nil {

			log.Error().Msg("Update failed")

			panic(retryErr)
		}
		log.Info().Msg("Deployment updated")
		if m["slackWebHookTokenUrl"] != "" {
			m["slackMessage"] = "DEPLOYMENT UPDATED"
			notifySlack(m)
		}
	}
}

// Check if deployment exists by app name
func checkDeploymentExistByAppName(m map[string]string) bool {
	deploymentsClient := clientset.AppsV1().Deployments(m["namespace"])
	fmt.Println(reflect.TypeOf(deploymentsClient))

	labelSelector := metav1.LabelSelector{
		MatchLabels: map[string]string{
			"app": m["appName"],
		},
	}

	fmt.Printf("Listing deployments %v in namespace %q:\n", labelSelector, apiv1.NamespaceDefault)
	list, err := deploymentsClient.List(context.TODO(), metav1.ListOptions{
		LabelSelector: labels.Set(labelSelector.MatchLabels).String(),
	})

	if err != nil {
		panic(err)
	}

	for _, d := range list.Items {
		fmt.Printf(" * %s (%d replicas)\n", d.Name, *d.Spec.Replicas)
		m["deployName"] = d.Name
		return true
	}
	return false
}

// Delete Deployment
func deleteDeployment(deployConfig DeploymentConfig, m map[string]string) {

	if checkDeploymentExistByAppName(m) {
		deploymentsClient := clientset.AppsV1().Deployments(m["namespace"])
		fmt.Println(reflect.TypeOf(deploymentsClient))
		deletePolicy := metav1.DeletePropagationForeground

		log.Info().
			Str("deployment", m["deleteDeploymentName"]).
			Msg("Deleting deployment...")

		if err := deploymentsClient.Delete(context.TODO(), m["deleteDeploymentName"], metav1.DeleteOptions{
			PropagationPolicy: &deletePolicy,
		}); err != nil {
			panic(err)
		}

		log.Info().Msg("Deployment deleted")

		m["service"] = m["appName"] + "-service"

		if m["slackWebHookTokenUrl"] != "" {
			m["slackMessage"] = "DEPLOYMENT DELETE"
			notifySlack(m)
		}
		deleteService(clientset, m)
	}
}

// Check if deployment exists by name
func checkDeploymentExistByDeployName(m map[string]string) bool {

	exist := true

	data, error := clientset.AppsV1().Deployments(m["namespace"]).Get(context.TODO(), m["deployName"], metav1.GetOptions{})

	if error != nil {
		log.Error().
			Str("Deployment", m["deployName"]).
			Str("Namespace", m["namespace"]).
			Msg("Deployment not found on k8s")

		exist = false
	}

	fmt.Println(data)

	return exist
}
