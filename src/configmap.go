package main

import (
	"context"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"os"
)

const configMapsJson = "configmap.json"

type ConfigMapsJson struct {
	Name       string            `json:"store_name"`
	ConfigMaps map[string]string `json:"configmaps"`
}

func getConfigMapData(m map[string]string ) ConfigMapsJson {

	var config ConfigMapsJson
	jsonPath := m["jsonpath"]

	// Open our jsonFile
	jsonFile, err := os.Open(jsonPath)
	// if we os.Open returns an error then handle it
	if err != nil {
		panic(err)
	}
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	jsonError := json.Unmarshal(byteValue, &config)

	if jsonError != nil {
		log.Error().Msg("Parsing json file: " + jsonPath + " failed")
		panic(jsonError)
	}

	log.Info().
		Str("ConfigmapName", config.Name).
		Msg("Getting Configmap values...")

	return config
}

/* Create/Update configmap and return its name */
func createConfigMaps(m map[string]string) string {

        config := getConfigMapData(m)

	configMap := apiv1.ConfigMap{
		TypeMeta: metav1.TypeMeta{
			Kind:       "ConfigMap",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      config.Name,
			Namespace: m["namespace"],
		},
		Data: config.ConfigMaps,
	}

	log.Info().Msg("ConfigMaps Structure: " + configMap.String())

	if _, err := clientset.CoreV1().ConfigMaps(m["namespace"]).Get(context.TODO(), config.Name, metav1.GetOptions{}); errors.IsNotFound(err) {
		_, createErr := clientset.CoreV1().ConfigMaps(m["namespace"]).Create(context.TODO(), &configMap, metav1.CreateOptions{})

		if createErr != nil {
			log.Error().Msg("Failed to create configmap")

			panic(err)
		} else {
			log.Info().Msg("Configmap created")
			if ( action == "config" ){
			   log.Info().Msg("Calling Updating Deploy " + action)
	                   m["configHash"] = createSha256Hash( m )
		           m["configmapName"] = config.Name
		           createDeploy(deployConfigs, m)
			}
		}
	}else {
	   updateConfigMaps(m)
	}
	return config.Name
}

func updateConfigMaps(m map[string]string) {

     config := getConfigMapData(m)

     configMap := apiv1.ConfigMap{
		TypeMeta: metav1.TypeMeta{
			Kind:       "ConfigMap",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      config.Name,
			Namespace: m["namespace"],
		},
		Data: config.ConfigMaps,
     }

     _, updateErr := clientset.CoreV1().ConfigMaps(m["namespace"]).Update(context.TODO(), &configMap, metav1.UpdateOptions{})

    if updateErr != nil {
        log.Error().Msg("Failed to update configmap")
        panic(updateErr)
    } else {
	log.Info().Msg("Configmap updated")
	if ( action == "config" ){
	   m["configHash"] = createSha256Hash( m )
	   m["configmapName"] = config.Name
	   if checkDeploymentExistByAppName(m) {
              if checkSecretRefExistByName(m) {
	           m["secretHash"] = createSha256Hash( m )
	      }
	      log.Info().Msg("Calling Updating Deploy " + action)
	      createDeploy(deployConfigs, m)
           }
	}
   }
}

func checkConfigMapExistByName(m map[string]string) bool {

	log.Info().Msg("Checking if ConfigMaps exist ")
        config := getConfigMapData(m)
	_, error := clientset.CoreV1().ConfigMaps(m["namespace"]).Get(context.TODO(), config.Name, metav1.GetOptions{})

	if error != nil {
		log.Error().
			Str("ConfigMapName", config.Name).
			Str("Namespace", m["namespace"]).
			Msg("ConfigMaps name not found on k8s")

		panic(error)
	}

	return true
}
