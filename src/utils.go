package main

import (
	"crypto/sha256"
	"fmt"
	"github.com/rs/zerolog/log"
	"math/rand"
	"os"
	"reflect"
)

// Convert int32
func int32Ptr(i int32) *int32 { return &i }

// Randon Suffix
const letterBytes = "abcdefghijklmnopqrstuvwxyz"

func randSuffix(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}

/* Check Environment Exist and is not empty */
func checkEnv(array interface{}) bool {

	envsArr := reflect.ValueOf(array)

	for i := 0; i < envsArr.Len(); i++ {
		value := envsArr.Index(i).Interface()
		_, status := os.LookupEnv(value.(string))

		valueStr := fmt.Sprintf("%v", value)

		if !status {
			log.Error().
				Str("variable-name", valueStr).
				Msg("Environment variable not found")

			return false

		} else if os.Getenv(valueStr) == "" {
			log.Error().
				Str("variable-name", valueStr).
				Msg("Environment variable is empty")
		}
	}

	array = nil
	return true
}

/* Check if json file exists under k8s config folder */
func checkK8sConfigJsonExist(filePath string) bool {

	result := true

	_, err := os.Stat(filePath)

	if err != nil {
		result = false
	}

	return result
}

func createSha256Hash( m map[string]string) string {
        h := fmt.Sprintln( m )
        hash := sha256.New()
        hash.Write([]byte( h ))
        h = fmt.Sprintf("%x", hash.Sum(nil))
        return ( h )
}

/*  Verify command line arguments related to action to be performed */
func checkComandLineArguments() bool {

	ok := true

	if len(os.Args) == 1 {
		panic("k8s-deploy: Action parameter expected: app | config | secret | version")

	} else if len(os.Args) > 1 {
		arg := os.Args[1]

		if arg != "app" && arg != "config" && arg != "secret" && arg != "delete" && arg != "version" {
			panic("k8s-deploy: Action parameter expected to be: app | config | secret | version")
		}
	}

	return ok
}
