package main

import (
	"github.com/rs/zerolog/log"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"strconv"
)

func createContainer(m map[string]string) []apiv1.Container {

	// default configmap is empty

	httpPort, err := strconv.Atoi(m["httpPort"])

	if err != nil {
		panic(err)
	}

	jsonPath := k8sConfigFolder + "/" + configMapsJson

	// check if configmap file is present to be used
	if checkK8sConfigJsonExist(jsonPath) && action != "config" {
		// this func comes from deployment, jsonPath for configmap is not set
		m["jsonpath"] = jsonPath

		// gets created/updated configmap name
		m["configmapName"] = createConfigMaps(m)
	}

	if deployConfigs.Pod.Container.SecretsRefName != "" && checkSecretRefExist(m, deployConfigs) && action != "secret"  {
		m["secretRefName"] = deployConfigs.Pod.Container.SecretsRefName
	}

	container := []apiv1.Container{
		{
			Name:            m["appName"],
			Image:           m["image"],
			ImagePullPolicy: apiv1.PullPolicy("Always"),
			EnvFrom:         getContainerEnvVars(m),
			Ports: []apiv1.ContainerPort{
				{
					Name:          "http-port",
					Protocol:      apiv1.ProtocolTCP,
					ContainerPort: int32(httpPort),
				},
			},
			ReadinessProbe: getContainerReadinessProbe(deployConfigs),
		},
	}

	return container
}

/* Return container en vars from secrets
and/or configmaps
*/
func getContainerEnvVars(m map[string]string) []apiv1.EnvFromSource {
	log.Info().
		Msg("Updating container")

	var envArray = []apiv1.EnvFromSource{}
	// In case it was created a configmap, It will create the configmaps struct for the container
	if m["configmapName"] != "" {
		cmRef := apiv1.EnvFromSource{
			ConfigMapRef: &apiv1.ConfigMapEnvSource{
				LocalObjectReference: apiv1.LocalObjectReference{
					Name: m["configmapName"],
				},
			},
		}
		envArray = append(envArray, cmRef)
	}

	// In case it was created a secret, It will create the secret struct for the container
	if m["secretRefName"] != "" {
		secretRef := apiv1.EnvFromSource{
			SecretRef: &apiv1.SecretEnvSource{
				LocalObjectReference: apiv1.LocalObjectReference{
					Name: m["secretRefName"],
				},
			},
		}
		envArray = append(envArray, secretRef)
	}
	return envArray
}

/*
	Return ReadinessProbe
 */
func getContainerReadinessProbe(config DeploymentConfig) *apiv1.Probe {
	var probe apiv1.Probe

	if config.Pod.Container.Probes.ReadinessProbe.HttpGet.Path != "" {
		probe = apiv1.Probe{
			InitialDelaySeconds: int32(config.Pod.Container.Probes.ReadinessProbe.InitialDelaySeconds),
			PeriodSeconds: int32(config.Pod.Container.Probes.ReadinessProbe.PeriodSeconds),
			Handler: apiv1.Handler{
				HTTPGet: &apiv1.HTTPGetAction{
					Path: config.Pod.Container.Probes.ReadinessProbe.HttpGet.Path,
					Port: intstr.IntOrString{
						Type:   intstr.Int,
						IntVal: int32(config.Pod.Container.Probes.ReadinessProbe.HttpGet.Port),
					},
				},
			},
		}
	}

	return &probe
}
