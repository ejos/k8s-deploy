package main

import (
	"context"
	"fmt"
	"github.com/rs/zerolog/log"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes"
	"reflect"
	"strconv"
)

// Create Service
func createService(m map[string]string) {

	log.Info().Msg("Creating service...")

	svcName := m["service"]
	httpPort, err := strconv.Atoi(m["httpPort"])

	if err != nil {
		panic(err)
	}

	deploymentsClient := clientset.CoreV1().Services(m["namespace"])
	service := &apiv1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      svcName,
			Namespace: m["namespace"],
			Labels: map[string]string{
				"app": m["appName"],
			},
		},
		Spec: apiv1.ServiceSpec{
			Ports: []apiv1.ServicePort{
				{
					Name:       "port",
					Protocol:   apiv1.ProtocolTCP,
					Port:       int32(httpPort),
					TargetPort: intstr.FromInt(httpPort),
				},
			},
			Selector: map[string]string{
				"app": m["appName"],
			},
			Type: "ClusterIP",
		},
	}
	result, err := deploymentsClient.Create(context.TODO(), service, metav1.CreateOptions{})

	if err != nil {
		panic(err)
	}

	log.Info().
		Str("service-name", result.GetObjectMeta().GetName()).
		Msg("Service created")
}

// Delete Service
func deleteService(clientset *kubernetes.Clientset, m map[string]string) {
	deploymentsClient := clientset.CoreV1().Services(m["namespace"])
	fmt.Println(reflect.TypeOf(deploymentsClient))
	deletePolicy := metav1.DeletePropagationForeground

	log.Info().
		Str("Service named: ", m["service"]).
		Msg("Will be Deleting")

	if err := deploymentsClient.Delete(context.TODO(), m["service"], metav1.DeleteOptions{
		PropagationPolicy: &deletePolicy,
	}); err != nil {
		panic(err)
	}

	log.Info().Msg("Service deleted")
}
